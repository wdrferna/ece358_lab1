from ctypes import resize
import random
import math
from enum import Enum

""" 
lmda = Average number of packets generated
rho = utlization of the queue
C = transmission rate (1 Mbps = 1000000 bps)
L = Average length of packet
k = size of buffer
E_N = Average size of the buffer
PIdle = proportion of time the server is idle as a probability
PLoss = proportion of packets lost comapred to arrivals
"""

# generates random variables based on exponential distribution
def genExpRandVar(lmda):
    U = random.random() # creates a random values from 0 to 1
    x = -(1/lmda)*math.log(1-U) # creates random variable with average 1/lmda
    return x

# calculation of lambda give utilization of queue
def calculateLambda(L, C, rho):
    return (rho * C) / L

# calculates service time with average size of packet = 2000 bits
def calculateServiceTime(L, C):
    return genExpRandVar(1/L)/C

class EventType(Enum):
    ARRIVAL = 0
    DEPARTURE = 1
    OBSERVER = 2

class MM1:
    def __init__(self, rho, simTime=1000):
        self.simTime = simTime
        self.eventList = [] # list that stores arrival, departure and observer events as tuples of (eventType, timestamp)
        self.rho = rho
        self.arrivals = 0
        self.departures = 0
        self.observers = 0

    # populate eventlist with arrivals, departures and observers
    def generateEvents(self, rho, simTime=1000):
        self.rho = rho
        self.eventList = []
        lmda = calculateLambda(2000, 1000000, self.rho)
        currTime =0
        # Generate arrival events until less than the simulation time
        while(currTime < simTime):            
            currTime += genExpRandVar(lmda)
            arrivalEvent = (EventType.ARRIVAL, currTime)
            if currTime <= simTime:
                self.eventList.append(arrivalEvent)
        
        # Generate departure events
        departureList = [] # get departure events into a seperate list for each arrival
        lastDepartureTime = 0
        for arrivalEvent in self.eventList:
            if lastDepartureTime > arrivalEvent[1]: # if the last packet in buffer leaves after the new packet arrives, new departure time = last departure time + sevice time
                lastDepartureTime += calculateServiceTime(2000, 1000000)
                if lastDepartureTime <= simTime:
                    departureList.append((EventType.DEPARTURE, lastDepartureTime))
            else: # if the last packet in buffer leaves before the new packet arrives, the new departure time = arrival time + service time
                lastDepartureTime = arrivalEvent[1] + calculateServiceTime(2000, 1000000) 
                if lastDepartureTime <= simTime:
                    departureList.append((EventType.DEPARTURE, lastDepartureTime))
        self.eventList.extend(departureList) # add the list of departures to the main eventList

        currTime = 0
        # Generate observer events at 5 times the lambda
        while(currTime < simTime):
            currTime += genExpRandVar(lmda*5)
            observerEvent = (EventType.OBSERVER, currTime)
            if currTime <= simTime:
                self.eventList.append(observerEvent) # add observers to eventList
        
        self.eventList.sort(key=lambda x: x[1]) # sort the events based on the timestamp of events

    def runSim(self):
        self.arrivals = 0
        self.departures = 0
        self.observers = 0
        bufferTotal = 0 # addition of buffer size at each observation event
        idleCount = 0 # number of times the buffer is empty at observer events
        for event in self.eventList:
            if event[0] == EventType.ARRIVAL:
                self.arrivals += 1
            elif event[0] == EventType.DEPARTURE:
                self.departures += 1
            elif event[0] == EventType.OBSERVER:
                self.observers += 1
                bufferSize = self.arrivals - self.departures # buffer size is difference between arrivals and departures
                bufferTotal += bufferSize
                if bufferSize == 0:
                    idleCount += 1

        E_N = bufferTotal / self.observers
        PIdle = idleCount / self.observers
        return (E_N, PIdle)

    def printlist(self):
        print(self.eventList)

class MM1K:
    def __init__(self, bufferSize, rho, simTime=1000):
        self.simTime = simTime
        self.bufferSize = bufferSize # size of queue = k
        self.eventList = [] # list of arrivals and observations
        self.rho = rho
        self.arrivals = 0
        self.departures = 0
        self.observers = 0
        self.pLoss = 0 # number of packets lost

    def generateEvents(self, rho, simTime=1000):
        self.rho = rho
        self.eventList = []
        lmda = calculateLambda(2000, 1000000, self.rho)
        currTime = 0
        # Generate arrival events
        while(currTime < simTime):            
            currTime += genExpRandVar(lmda)
            arrivalEvent = (EventType.ARRIVAL, currTime)
            if currTime <= simTime:
                self.eventList.append(arrivalEvent)

        currTime = 0
        # Generate observer events at 5 times the arrival rate
        while(currTime < simTime):
            currTime += genExpRandVar(lmda*5)
            observerEvent = (EventType.OBSERVER, currTime)
            if currTime <= simTime:
                self.eventList.append(observerEvent)
        
        self.eventList.sort(key=lambda x: x[1]) # sort the list of arrivals and observers

    def runSim(self):
        self.arrivals = 0
        self.departures = 0
        self.observers = 0
        self.pLoss = 0
        bufferTotal = 0
        idleCount = 0 # number of times the queue is empty or full
        currBuffSize = 0
        departureQueue = [] # list to store future departure events
        for event in self.eventList:
            if currBuffSize != 0: # at each arrival/observer event, go through departure queue and process/remove departure events with times less than current event
                while(currBuffSize != 0 and departureQueue[0][1] < event[1]):
                    departureQueue.pop(0)
                    currBuffSize -= 1
                    self.departures += 1
            if event[0] == EventType.ARRIVAL:
                #print("buff Size:", currBuffSize)
                if currBuffSize < self.bufferSize:
                    departureTime = 0
                    if currBuffSize == 0: # if buffer is empty, departure time of current packet = arrival time + service time
                        departureTime = event[1] + calculateServiceTime(2000, 1000000)
                    else: # if buffer is not empty, departure time = departure time of last packet + service time
                        departureTime = departureQueue[-1][1] + calculateServiceTime(2000, 1000000)
                    departureQueue.append((EventType.DEPARTURE, departureTime)) # adds departure packet to departure queue
                    self.arrivals += 1
                    currBuffSize += 1
                elif currBuffSize == self.bufferSize: # if buffer is full during arrival, packet is lost
                    self.arrivals += 1
                    self.pLoss += 1
            elif event[0] == EventType.OBSERVER:
                self.observers += 1
                bufferTotal += currBuffSize
                if currBuffSize == 0 or currBuffSize == self.bufferSize: # increase idle count if buffer size is zero or the buffer is full
                    idleCount += 1
                

        E_N = bufferTotal / self.observers
        PIdle = idleCount / self.observers
        PLoss = (self.pLoss / self.arrivals) * 100 # percentage P Loss
        return (E_N, PIdle, PLoss)

# Question 1 - verify generated mean and expected mean is similar
def verifyRandomGen():
    print("/-----------Question 1-----------/\n")
    lmda = 75
    meanTotal = 0
    varTotal = 0
    expectedMean = 1/lmda
    expectedVariance = 1/pow(lmda, 2)

    for i in range(1000):
        x = genExpRandVar(lmda)
        meanTotal += x # total for mean
        varTotal += (x-expectedMean)**2 # total for variance
    
    calculatedMean = meanTotal/1000
    calculatedVariance = varTotal/1000
    print("Calculated mean: ", calculatedMean)
    print("Expected mean  : ", expectedMean)
    print("Calculated variance: ", calculatedVariance)
    print("Expected variance  : ", expectedVariance, "\n")


# Verify stablity of simulator for MM1
def verifySimStabilityMM1():
    print("/-----------Simulator stability verification MM1-----------/")
    print("Percentage difference of E_N and P_Idle between T=1000 and T=2000\n")
    rho = 0.25
    while(rho <= 0.95):
        mm1_T = MM1(rho) # create MM1 simulator instance for T = 1000
        mm1_T.generateEvents(rho)
        mm1_2T = MM1(rho, 2000) # create MM1 simulator instance for T = 2000
        mm1_2T.generateEvents(rho, 2000)
        T_results = mm1_T.runSim()
        T2_results = mm1_2T.runSim()
        EN_Diff = (abs(T_results[0]-T2_results[0])/T_results[0])*100 # calculates % difference between E_N values
        PIdle_Diff = (abs(T_results[1]-T2_results[1])/T_results[1])*100 # calculates % difference between P_Idle values
        print("rho: ", round(rho,2), ", " "E_N: ", EN_Diff, ", P_Idle: ", PIdle_Diff)
        rho += 0.1
    print("\n")

# Question 3 - run simulator for each rho value specified and presents E_N and P_Idle values
def genSimResultsMM1():
    print("/-----------Question 3-----------/\n")
    rho = 0.25
    while(rho <= 0.95):
        mm1 = MM1(rho)
        mm1.generateEvents(rho)
        results = mm1.runSim()
        print("rho: ", round(rho,2), ", ", "E_N: ", results[0], ", ", "P_Idle: ", results[1])
        rho += 0.1
    print("\n")

# Question 4 - run simulator for rho = 1.2 and presents E_N and P_Idle values
def simForRho1_2():
    print("/-----------Question 4-----------/\n")
    rho = 1.2
    mm1 = MM1(rho)
    mm1.generateEvents(rho)
    results = mm1.runSim()

    print("rho: ", round(rho,2), ", ", "E_N: ", results[0], ", ", "P_Idle: ", results[1], "\n")

# Verify stablity of simulator for MM1K
def verifySimStabilityMM1K():
    print("/-----------Simulator stability verification MM1K-----------/")
    print("Percentage difference of E_N, P_Idle and P_Loss between T=2000 and T=3000\n")
    kList = [10, 25, 50] # list of k values to iterate through
    for k in kList:
        rho = 0.5
        print("K = ", k)
        while(rho <= 1.6):
            mm1k_T = MM1K(k, rho, 2000) # create simulator instance for T
            mm1k_T.generateEvents(rho, 2000)
            mm1k_2T = MM1K(k, rho, 3000) # create simulator instance for 2T
            mm1k_2T.generateEvents(rho, 3000)
            # run simulators and get % diff for E_N P_Idle and P_Loss values for each rho value
            T_results = mm1k_T.runSim()
            T2_results = mm1k_2T.runSim()
            EN_Diff = (abs(T_results[0]-T2_results[0])/T_results[0])*100
            PIdle_Diff = (abs(T_results[1]-T2_results[1])/T_results[1])*100
            PLoss_Diff = 0
            if round(T_results[2], 0) != 0:
                PLoss_Diff = (abs(T_results[2]-T2_results[2])/T_results[2])*100
            print("rho: ", round(rho,2), ", " "E_N: ", EN_Diff, ", P_Idle: ", PIdle_Diff, ", P_Loss: ", PLoss_Diff)
            rho += 0.1
        print("\n")

# Question 6 - run MM1K simulator for multiple K values with different rho values and present E_N, P_Idle and P_Loss values
def MM1KsimRun():
    print("/-----------Question 6-----------/\n")
    kList = [10, 25, 50]
    for k in kList:
        rho = 0.5
        print("K = ", k)
        while(rho < 1.6):
            mm1k = MM1K(k, rho, 2000) # create MM1K simulator instance
            mm1k.generateEvents(rho, 2000) # generate eventlist
            results = mm1k.runSim() # run simulator
            print("rho: ", round(rho,2), ", ", "E_N: ", results[0], ", ", "P_Idle: ", results[1], ", ", "P_Loss: ", results[2]/100)
            rho += 0.1
        print("\n")

def main():
    # Question 1
    verifyRandomGen()
  
    # Verify stability of simulator for MM1
    verifySimStabilityMM1()

    # Question 3
    genSimResultsMM1()

    # Question 4
    simForRho1_2()

    # Verify stability of simulator for MM1K
    verifySimStabilityMM1K()

    # Question 6
    MM1KsimRun()

if __name__ == '__main__':
    main()

